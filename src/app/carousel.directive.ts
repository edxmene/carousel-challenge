import {
  AfterViewInit,
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[appCarousel]',
})
export class CarouselDirective implements OnInit, AfterViewInit {
  @Input('appCarousel') items!: any[];
  i: number = 0;

  ////////////////////////////////////////////////////////
  @ViewChild('Prev') buttomPrev!: ElementRef;
  @ViewChild('Next') buttomNext!: ElementRef<HTMLElement>;
  ////////////////////////////////////////////////////////
  constructor(
    private viewContainerRef: ViewContainerRef,
    private template: TemplateRef<any>,
    private rederer: Renderer2
  ) {}

  ngOnInit(): void {
    console.log(this.template.elementRef.nativeElement);
    this.viewContainerRef.createEmbeddedView(this.template, {
      img: this.getImg(),
    });

    const button1 = this.rederer.selectRootElement('#Prev');
    this.rederer.listen(button1, 'click', () => {
      this.getPrev();
    });
    this.rederer.setProperty(button1, 'innerHTML', 'Previous');

    const button2 = this.rederer.selectRootElement('#Next');
    this.rederer.listen(button2, 'click', () => {
      this.getNext();
    });
    this.rederer.setProperty(button2, 'innerHTML', 'Next');
  }

  ngAfterViewInit(): void {
    // console.log(this.buttomNext);
  }

  getImg() {
    return this.items[this.i].image;
  }

  getPrev() {
    this.i = this.i === 0 ? 0 : this.i - 1;
    this.viewContainerRef.clear();
    this.viewContainerRef.createEmbeddedView(this.template, {
      img: this.getImg(),
    });
    console.log(this.i);
  }
  //edit here
  getNext() {
    this.i = this.i === this.items.length - 1 ? this.i : this.i + 1;
    this.viewContainerRef.clear();
    this.viewContainerRef.createEmbeddedView(this.template, {
      img: this.getImg(),
    });
    console.log(this.i);
  }
}

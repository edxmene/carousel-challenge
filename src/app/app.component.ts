import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'directive-challenge';

  imgCollection: Array<object> = [
    {
      image: 'assets/cat.jpg',
      thumbImage: 'assets/glasses.jpg',
      alt: 'Image 1',
      title: 'Image 1',
    },
    {
      image: 'assets/dog.jpg',
      thumbImage: 'assets/glasses.jpg',
      title: 'Image 2',
      alt: 'Image 2',
    },
    {
      image: 'assets/kitten.jpg',
      thumbImage: 'assets/glasses.jpg',
      title: 'Image 4',
      alt: 'Image 4',
    },
    {
      image: 'assets/dog2.jpg',
      thumbImage: 'assets/glasses.jpg',
      title: 'Image 4',
      alt: 'Image 4',
    },
  ];

  ngOnInit(): void {}

  slides: string[] = [
    './assets/cat.jpg',
    './assets/dog.jpg',
    './assets/kitten.jpg',
    './assets/glasses.jpg',
  ];
  i: number = 2;

  getSlide() {
    return this.slides[this.i];
  }

  getPrev() {
    this.i = this.i === 0 ? 0 : this.i - 1;
  }
  //edit here
  getNext() {
    this.i = this.i === this.slides.length - 1 ? this.i : this.i + 1;
  }
}

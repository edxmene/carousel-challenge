import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HideAfterDirective } from './hide-after.directive';
import { NgImageSliderModule } from 'ng-image-slider';
import { CarouselDirective } from './carousel.directive';
import { CarouselImageDirective } from './carousel-image.directive';

@NgModule({
  declarations: [AppComponent, HideAfterDirective, CarouselDirective, CarouselImageDirective],
  imports: [BrowserModule, NgImageSliderModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

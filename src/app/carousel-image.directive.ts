import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appCarouselImage]',
})
export class CarouselImageDirective implements OnInit {
  constructor(private elRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit(): void {
    console.log(this.elRef.nativeElement);
    this.renderer.addClass(this.elRef.nativeElement, 'carousel-section');

    const button1 = this.renderer.selectRootElement('#Prev');
    this.renderer.addClass(button1, 'button1');

    const button2 = this.renderer.selectRootElement('#Next');
    this.renderer.addClass(button2, 'button2');
  }
}

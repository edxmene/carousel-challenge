import {
  Directive,
  Input,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';

@Directive({
  selector: '[HideAfter]',
})
export class HideAfterDirective implements OnInit {
  @Input('HideAfter') delay = 0;
  @Input('HideAfterThen') placeHolder!: TemplateRef<any>;

  constructor(
    private viewContainerRef: ViewContainerRef,
    private template: TemplateRef<any>
  ) {}

  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.template);
    setTimeout(() => {
      this.viewContainerRef.clear();
      if (this.placeHolder) {
        this.viewContainerRef.createEmbeddedView(this.placeHolder);
      }
    }, this.delay);
  }
}
